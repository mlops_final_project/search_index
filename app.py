import json
import logging
import os
from typing import List

import flask
import joblib
import numpy as np
from flask import Flask, jsonify
from scipy import linalg, dot

CLUSTER_ID = os.environ["CLUSTER_ID"]
K_NEIGHBOURS = int(os.environ["K_NEIGHBOURS"])
INDEX = joblib.load(f"/data/cluster_{CLUSTER_ID}/index")
with open(f"/data/cluster_{CLUSTER_ID}/documents_db.json") as f:
    DOCUMENTS_DB = json.load(f)

logging.getLogger().setLevel(logging.INFO)
app = Flask(__name__)


def get_top_indices(embedding: np.array, neighbours_embeddings: np.array, top_k: int) -> np.array:
    cos_similarities = dot(neighbours_embeddings, embedding.T) / linalg.norm(neighbours_embeddings) / linalg.norm(
        embedding)
    cos_similarities = cos_similarities.flatten()
    sorted_indices = np.argsort(cos_similarities)
    top_indices = sorted_indices[:min(top_k, cos_similarities.shape[0])]
    scores = cos_similarities[top_indices]
    return top_indices, scores


def prepare_data(embedding: List[float]) -> np.array:
    embedding = np.array(embedding, dtype="float32")
    embedding = embedding.reshape(1, -1)
    return embedding


@app.route("/get_k_neighbours", methods=["POST", "GET"])
def get_k_neighbours():
    data = flask.request.get_json(force=True)
    embedding = data["embedding"]
    top_k = data["top_k"]
    embedding = prepare_data(embedding=embedding)
    _, indices = INDEX.search(embedding, K_NEIGHBOURS)
    indices = indices.flatten()
    neighbours_embeddings = [INDEX.reconstruct(int(index_)) for index_ in indices]
    neighbours_embeddings = np.array(neighbours_embeddings, dtype="float32")
    top_indices, scores = get_top_indices(embedding=embedding, neighbours_embeddings=neighbours_embeddings, top_k=top_k)
    top_real_indices = indices[top_indices]
    duplicates = [(DOCUMENTS_DB[index_], float(score)) for index_, score in zip(top_real_indices, scores)]
    return jsonify(duplicates=duplicates)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)
