import json
import logging
import os
import pickle
from typing import List, Dict

import faiss
import joblib
import numpy as np

logging.getLogger().setLevel(logging.INFO)


def save_documents(output_dir: str, documents: List[str]) -> None:
    documents_output_path = os.path.join(output_dir, "documents_db.json")
    with open(documents_output_path, "w") as fw:
        json.dump(documents, fw)


def create_index(documents: List[str], clusters_embeddings: Dict[str, np.array]) -> faiss.IndexFlatL2:
    documents_num = len(documents)
    documents_embeddings = np.vstack([
        clusters_embeddings[documents[document_id]]
        for document_id
        in range(documents_num)
    ])
    index = faiss.IndexFlatL2(512)
    index.add(documents_embeddings)
    return index


def save_index(output_dir: str, index: faiss.IndexFlatL2) -> None:
    index_output_path = os.path.join(output_dir, "index")
    joblib.dump(index, index_output_path)


def prepare_search_index(
    data_dir: str,
    processed_data_dir: str,
    generations: List[int],
    clusters_num: int = 4,
) -> None:
    for generation in generations:
        logging.info(f"Start data loading - generation: {generation}")
        documents_input_path = os.path.join(data_dir, f"clusters_use_dg{generation}.json")
        with open(documents_input_path) as fr:
            clusters_documents = json.load(fr)
        embeddings_input_path = os.path.join(data_dir, f"use_embeddings_dg{generation}.pkl")
        with open(embeddings_input_path, "rb") as fr:
            clusters_embeddings = pickle.load(fr)
        logging.info("End data loading")
        for cluster_id in range(clusters_num):
            logging.info(f"Start create index - cluster_id: {cluster_id}, generation: {generation}")
            output_dir = os.path.join(processed_data_dir, f"generation_{generation}", f"cluster_{cluster_id}")
            os.makedirs(output_dir, exist_ok=True)
            cluster_id_str = str(cluster_id)

            documents = clusters_documents[cluster_id_str]
            save_documents(output_dir=output_dir, documents=documents)

            index = create_index(documents=documents, clusters_embeddings=clusters_embeddings)
            save_index(output_dir=output_dir, index=index)
            logging.info("End create index")


if __name__ == '__main__':
    prepare_search_index(
        data_dir="/root/mlops_final_project/infra/data",
        processed_data_dir="/root/mlops_final_project/infra/processed_data",
        generations=[1, 2],
        clusters_num=4,
    )
